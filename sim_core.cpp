//
// ® Pipeline MIPS ® - ® Roy Maor & Ofir Israel April 2018 ®
//

/* 046267 Computer Architecture - SPR_temping 2017 - HW #1 */
/* This file should hold your implementation of the CPU pipeline core simulator */

/********use Replace to get from //cout to cout and vice versa! saves time for tests****************/


/************************Includes:************************************/

#include "sim_api.h"
#include <iostream>

/************************Namespaces:************************************/

using namespace std;

/************************Defines:************************************/

#define SUCCESS 0
#define FAILURE -1

/***********************External Variables:**************************/

extern SIM_cmd instructions[100];


/************************Structs:************************************/

typedef struct {
	bool jump;
	int32_t readResult; //
	int32_t dstReg;
	int32_t lastUsedDstReg;
	int32_t exResult;  /// Result of ALU saved after arithmetic action
} PipeRegisters;


/**********************Global Variables:******************************/

SIM_coreState myState;
PipeRegisters PR_temp; // temp var saving last cycles results
PipeRegisters PR; //new result are inserted here
int memFail = 0;


/************************Enums:************************************/

typedef enum {
	NO_DEP = 0,DEC_EXE, DEC_EXE_BRDST, DEC_MEM, DEC_WB, STORE_DOUBLE
} StallDependencyType;

typedef enum {
	EXE_NO_DEP = 0, EXE_MEM_NOT_LOAD, EXE_MEM_LOAD, EXE_WB, EXE_WB_AND_MEM
} FWDependencyType;


/**********************General Functions:*******************************/
/*! insertNOP: Insert a nope command into the stage
  \param[in] stage you want to insert NOP command into
*/
void insertNOP(pipeStage stage) {
	myState.pipeStageState[stage].cmd.opcode = CMD_NOP;
	myState.pipeStageState[stage].cmd.src1 = 0;
	myState.pipeStageState[stage].cmd.src2 = 0;
	myState.pipeStageState[stage].cmd.isSrc2Imm = 0;
	myState.pipeStageState[stage].cmd.dst = 0;
	myState.pipeStageState[stage].src1Val = 0;
	myState.pipeStageState[stage].src2Val = 0;
}

/*! flush: Insert nope to all pipe stages
*/
void flush(){ //advance current MEM to WB, insert NOPS to DECODE,EXE,MEM, and get new instruction by FETCH
	//cout << "in flush!" << endl;
	insertNOP(FETCH);
	insertNOP(DECODE);
	insertNOP(EXECUTE);
}


/*! isJumpCommand: Checks whether an opc is one of the jump command
  \param[in] opc - Opcode of the checked command 
*/
bool isJumpCommand(SIM_cmd_opcode opc){
	if(opc == CMD_BRNEQ || opc == CMD_BR || opc == CMD_BREQ)
		return true;
	return false;
}


/**********************Stall Dependency Check:*******************************/

/*! detectDependency: returns a paramter of the Stall dependency of the pipe
*/
StallDependencyType detectDependency(){
	int Dsrc1 = 0, Dsrc2 = -1; // -1 in case of immediate
	int dstD = 0, dstE = 0, dstM = 0, dstWB = 0;
	Dsrc1 = myState.pipeStageState[DECODE].cmd.src1;
	if (!myState.pipeStageState[DECODE].cmd.isSrc2Imm) // if src2 is register index
		Dsrc2 = myState.pipeStageState[DECODE].cmd.src2; //else -1
		
	dstD = myState.pipeStageState[DECODE].cmd.dst;
	dstE = myState.pipeStageState[EXECUTE].cmd.dst;
	dstM = myState.pipeStageState[MEMORY].cmd.dst;
	dstWB = myState.pipeStageState[WRITEBACK].cmd.dst;
	
	SIM_cmd_opcode opcD = myState.pipeStageState[DECODE].cmd.opcode;
	SIM_cmd_opcode opcE = myState.pipeStageState[EXECUTE].cmd.opcode;
	SIM_cmd_opcode opcM = myState.pipeStageState[MEMORY].cmd.opcode;
	SIM_cmd_opcode opcWB = myState.pipeStageState[WRITEBACK].cmd.opcode;
	
	if(opcD == CMD_NOP || opcD == CMD_HALT)
		return NO_DEP;
	if (((Dsrc1 ==   dstE || Dsrc2 == dstE) && !(opcE == CMD_NOP) && !(opcE == CMD_BREQ) && !(opcE == CMD_BRNEQ)) || (dstD == dstE && opcE == CMD_STORE) || (dstD == dstE && opcD == CMD_BREQ) || (dstD == dstE && opcD == CMD_BRNEQ))
		return DEC_EXE;
	else if (((Dsrc1 == dstM || Dsrc2 == dstM) && !(opcM == CMD_NOP) && !(opcM == CMD_BREQ) && !(opcM == CMD_BRNEQ)) || (dstD == dstM && opcM == CMD_STORE) || (dstD == dstM && opcD == CMD_BREQ) || (dstD == dstM && opcD == CMD_BRNEQ))
	{
		return DEC_MEM;
	}
	else if (((Dsrc1 == dstWB || Dsrc2 == dstWB) && !(opcWB == CMD_NOP) && !(opcWB == CMD_BREQ) && !(opcWB == CMD_BRNEQ)) || (dstD == dstWB && opcD == CMD_STORE) || (dstD == dstWB && opcD == CMD_BREQ) || (dstD == dstWB && opcD == CMD_BRNEQ))
		return DEC_WB;
	
	//special check for branches
	if(isJumpCommand(opcD)){
		//cout << "in special branch check " << dstM << endl;
		int dstD = myState.pipeStageState[DECODE].cmd.dst;
			if(dstD == dstE)
				return DEC_EXE_BRDST; //this dependency occurs when dst of branch is not determined in time.
			if(dstD == dstM)
				return DEC_EXE_BRDST;
		if(!forwarding && !split_regfile){ //only in STALL mode. in split, this situation does need require a stall
			if(dstD == dstWB)
				return DEC_EXE_BRDST;
		}
	}


	return NO_DEP;
}


/**********************Forwarding Dependency Check:*******************************/

/*! detectForwardDependency: returns a paramter of the Forward dependency of the pipe
*/
FWDependencyType detectForwardDependency() {
	int dstM = myState.pipeStageState[MEMORY].cmd.dst;
	int dstWB = myState.pipeStageState[WRITEBACK].cmd.dst;
	int Esrc1 = myState.pipeStageState[EXECUTE].cmd.src1;
	int Esrc2 = -1; // -1 in case of immediate
	if (!myState.pipeStageState[EXECUTE].cmd.isSrc2Imm) // if src2 is register index
		Esrc2 = myState.pipeStageState[EXECUTE].cmd.src2; //else -1

	SIM_cmd_opcode opcD = myState.pipeStageState[DECODE].cmd.opcode;
	SIM_cmd_opcode opcE = myState.pipeStageState[EXECUTE].cmd.opcode;
	SIM_cmd_opcode opcM = myState.pipeStageState[MEMORY].cmd.opcode;
	SIM_cmd_opcode opcWB = myState.pipeStageState[WRITEBACK].cmd.opcode;

		
	
	if(opcE != CMD_NOP && opcE != CMD_HALT){
		
		
		if ((opcM != CMD_NOP && opcM != CMD_HALT && opcM != CMD_BREQ && opcM != CMD_BRNEQ) && (opcWB != CMD_NOP && opcWB != CMD_HALT && opcWB != CMD_BREQ && opcWB != CMD_BRNEQ))
			if ((Esrc1 == dstWB || Esrc2 == dstWB) && (Esrc1 == dstM || Esrc2 == dstM))
				return EXE_WB_AND_MEM;
		
		
		//EXE_MEM
		if (opcM != CMD_NOP && opcM != CMD_HALT && opcM != CMD_BREQ && opcM != CMD_BRNEQ){
			if (Esrc1 == dstM || Esrc2 == dstM) {
				if (opcM == CMD_LOAD)
					return EXE_MEM_LOAD;
				else
					return EXE_MEM_NOT_LOAD;
			}
		}

		if (opcWB != CMD_NOP && opcWB != CMD_HALT && opcWB != CMD_BREQ && opcWB != CMD_BRNEQ){
			if (Esrc1 == dstWB || Esrc2 == dstWB)
				return EXE_WB;
		}
	}

	return EXE_NO_DEP;
}

/**********************Forwarding Mechanism:*******************************/

/*! Forward: Forward handler, forwarding registers info by forward dependency
  \param[in] FWDdep - THe forward dependency need to be handled
*/
void Forward(FWDependencyType FWDdep) {
	int dstM = myState.pipeStageState[MEMORY].cmd.dst;
	int dstWB = myState.pipeStageState[WRITEBACK].cmd.dst;
	int Esrc1 = myState.pipeStageState[EXECUTE].cmd.src1;
	int Esrc2 = -1; // -1 in case of immediate
	if (!myState.pipeStageState[EXECUTE].cmd.isSrc2Imm) // if src2 is register index
		Esrc2 = myState.pipeStageState[EXECUTE].cmd.src2; //else -1

	
	if (FWDdep == EXE_WB || FWDdep == EXE_WB_AND_MEM) {
		if (Esrc1 == dstWB){
			myState.pipeStageState[EXECUTE].src1Val = PR.readResult;
		}
		if (Esrc2 == dstWB){
			myState.pipeStageState[EXECUTE].src2Val = PR.readResult;
		}
	}
	
	if (FWDdep == EXE_MEM_NOT_LOAD || FWDdep == EXE_WB_AND_MEM) {
		if (Esrc1 == dstM){
			myState.pipeStageState[EXECUTE].src1Val = PR.exResult;
		}
		if (Esrc2 == dstM){
			myState.pipeStageState[EXECUTE].src2Val = PR.exResult;
		}
	}
}



/*! checkDecodeStore: Check whether Store command is now in "Decode Stage" and there is DEC_MEM dependency
*/
int checkDecodeStore()
{
	SIM_cmd_opcode opcD = myState.pipeStageState[DECODE].cmd.opcode;
	if (opcD != CMD_STORE)
		return 0;
	
	int Dsrc1, Dsrc2=-1, Ddst;
	Dsrc1 = myState.pipeStageState[DECODE].cmd.src1;
	if (!myState.pipeStageState[DECODE].cmd.isSrc2Imm) // if src2 is register index
		Dsrc2 = myState.pipeStageState[DECODE].cmd.src2; //else -1
	Ddst = myState.pipeStageState[DECODE].cmd.dst;
	int dstM = myState.pipeStageState[MEMORY].cmd.dst;

	
	if (dstM == Ddst || dstM == Dsrc1 || dstM == Dsrc2)
		return 1;
	else
		return 0;

}
/**********************Split Mechanism:*******************************/

/*! Split: Split handler
*/
void Split(){
	int Dsrc1 = myState.pipeStageState[DECODE].cmd.src1;
	int dstD = myState.pipeStageState[DECODE].cmd.dst;
	int dstWB = myState.pipeStageState[WRITEBACK].cmd.dst;
	int Dsrc2 = -1;
	if (!myState.pipeStageState[DECODE].cmd.isSrc2Imm) // if src2 is register index
		Dsrc2 = myState.pipeStageState[DECODE].cmd.src2; //else -1

	if (myState.pipeStageState[WRITEBACK].cmd.dst != 0) {
		myState.regFile[myState.pipeStageState[WRITEBACK].cmd.dst] = PR.readResult;
	}
	if (Dsrc1 == dstWB) {
		myState.pipeStageState[DECODE].src1Val = myState.regFile[Dsrc1];
		myState.regFile[Dsrc1] = PR.readResult;
	}
	if (Dsrc2 == dstWB && !(myState.pipeStageState[WRITEBACK].cmd.opcode == CMD_NOP)) {
		myState.pipeStageState[DECODE].src2Val = myState.regFile[Dsrc2];
		myState.regFile[Dsrc2] = PR.readResult;
	}
	
	if (dstD == dstWB && myState.pipeStageState[DECODE].cmd.opcode == CMD_STORE)
	{
		myState.regFile[dstD] = PR.readResult;
	}
	
}

/**********************Pipeline Stages:*******************************/

int WriteBack(){
	SIM_cmd_opcode opc = myState.pipeStageState[WRITEBACK].cmd.opcode;
	switch (opc) {
		case CMD_NOP:
			break;
		case CMD_ADD:
		case CMD_SUB:
		case CMD_ADDI:
		case CMD_SUBI:
		case CMD_LOAD:
			if (myState.pipeStageState[WRITEBACK].cmd.dst !=0) //never write to $0 register
				myState.regFile[myState.pipeStageState[WRITEBACK].cmd.dst] = PR_temp.readResult;
			break;
		case CMD_STORE:
		case CMD_BR:
		case CMD_BREQ:
		case CMD_BRNEQ:
			break;
		case CMD_HALT:
			break;
	}
	return 0;
}


int Memory(){
	SIM_cmd_opcode opc = myState.pipeStageState[MEMORY].cmd.opcode;
	switch (opc) {
		case CMD_NOP:
			break;
		case CMD_ADD:
		case CMD_SUB:
		case CMD_ADDI:
		case CMD_SUBI:
			PR.readResult = PR_temp.exResult;
			break;
		case CMD_LOAD:
			return SIM_MemDataRead(PR_temp.exResult,&PR.readResult); // (dst <- Mem[src1 + src2])
		case CMD_STORE:
			SIM_MemDataWrite(PR_temp.exResult,myState.pipeStageState[MEMORY].src1Val); // Mem[dst + src2] <- src1
			break;
		case CMD_BR:
		case CMD_BREQ:
		case CMD_BRNEQ:
			if (PR_temp.jump) //jumping occurs in MEM stage
			{
				PR.jump = false; //reset jump flag. PR_temp will be set to false in next cycle. right now PR_temp indicates a jump is happening
				if(forwarding){
					int newVal = myState.regFile[myState.pipeStageState[MEMORY].cmd.dst];
					myState.pc = PR_temp.exResult - PR_temp.lastUsedDstReg + newVal; //fix miscalculation from EXE stage
				}
				else
					myState.pc=PR_temp.exResult;
				flush();
			}
			return 0;
		case CMD_HALT:
			break;
	}
	return 0;
}


void Execute(){
	SIM_cmd_opcode opc = myState.pipeStageState[EXECUTE].cmd.opcode;
	int src1val = myState.pipeStageState[EXECUTE].src1Val;
	int src2val = myState.pipeStageState[EXECUTE].src2Val;
	int dst = myState.pipeStageState[EXECUTE].cmd.dst;
	switch (opc) {
		case CMD_NOP:
			break;
		case CMD_ADD:
		case CMD_ADDI:
			PR.exResult = src1val + src2val;
			break;
		case CMD_SUB:
		case CMD_SUBI:
			PR.exResult = src1val - src2val;
			break;
		case CMD_LOAD:
			PR.exResult = src1val + src2val; // (dst <- Mem[src1 + src2])
			break;
		case CMD_STORE:
			PR.exResult = dst + src2val; // Mem[dst + src2] <- src1
			break;
		case CMD_BREQ:
			if (src1val == src2val)
				PR.jump=true;
			else
				PR.jump = false;
			PR.exResult =  (myState.pc-2*4) + PR_temp.dstReg; //get base jump address from FETCH stage
			PR.lastUsedDstReg = PR_temp.dstReg;
			break;
		case CMD_BRNEQ:
			if (src1val != src2val)
				PR.jump=true;
			else
				PR.jump = false;
			PR.exResult =  (myState.pc-2*4) + PR_temp.dstReg; //get base jump address from FETCH stage
			PR.lastUsedDstReg = PR_temp.dstReg;
			break;
		case CMD_BR:
			PR.jump=true;
			PR.exResult =  (myState.pc-2*4) + PR_temp.dstReg; //get base jump address from FETCH stage
			PR.lastUsedDstReg = PR_temp.dstReg;
			break;
		case CMD_HALT:
			break;
	}
}


void Decode(){
	SIM_cmd_opcode opc = myState.pipeStageState[DECODE].cmd.opcode;
	switch (opc) {
		case CMD_NOP:
			insertNOP(DECODE);
			break;
		case CMD_ADD:
		case CMD_SUB:
		case CMD_BR: //no register read (unconditional jump)
		case CMD_BREQ:
		case CMD_BRNEQ:
			PR.dstReg = myState.regFile[myState.pipeStageState[DECODE].cmd.dst];
			myState.pipeStageState[DECODE].src1Val = myState.regFile[myState.pipeStageState[DECODE].cmd.src1];
			myState.pipeStageState[DECODE].src2Val = myState.regFile[myState.pipeStageState[DECODE].cmd.src2];
			break;
		case CMD_ADDI:
		case CMD_SUBI:
			myState.pipeStageState[DECODE].src1Val = myState.regFile[myState.pipeStageState[DECODE].cmd.src1];
			myState.pipeStageState[DECODE].src2Val = myState.pipeStageState[DECODE].cmd.src2;
			break;
		case CMD_LOAD:
		case CMD_STORE:
			myState.pipeStageState[DECODE].src1Val = myState.regFile[myState.pipeStageState[DECODE].cmd.src1];
			if(myState.pipeStageState[DECODE].cmd.isSrc2Imm)
				myState.pipeStageState[DECODE].src2Val = myState.pipeStageState[DECODE].cmd.src2;
			else
				myState.pipeStageState[DECODE].src2Val = myState.regFile[myState.pipeStageState[DECODE].cmd.src2];
			break;
		case CMD_HALT: //no register read
			break;
	}
}


void Fetch(){
	myState.pc += 4;
	myState.pipeStageState[FETCH].cmd = instructions[myState.pc/4]; //new instruction
	//update decode read values
	myState.pipeStageState[DECODE].src1Val = myState.regFile[myState.pipeStageState[DECODE].cmd.src1];
	if (myState.pipeStageState[DECODE].cmd.isSrc2Imm)
		myState.pipeStageState[DECODE].src2Val = myState.pipeStageState[DECODE].cmd.src2;
	else
		myState.pipeStageState[DECODE].src2Val = myState.regFile[myState.pipeStageState[DECODE].cmd.src2];
}

/**********************Pipeline Stage Shift:*******************************/

/*! shiftPipe: shiting the pipeline by the stage provided. if Fetch -> inserts a new instruction.
  \param[in] stage - The stage to shift from
*/
void shiftPipe(pipeStage stage){
	for(int i=SIM_PIPELINE_DEPTH-1; i>stage; i--)
		myState.pipeStageState[i]=myState.pipeStageState[i-1];
	if (stage == FETCH || PR_temp.jump){
		Fetch();
	}
	else
		insertNOP(stage);
}


/**********************Core Reset:*******************************/
/*! SIM_CoreReset: Resets the simulator pipe
*/
int SIM_CoreReset(void) {
	myState.pc = 0;
	for(int i=0; i<SIM_REGFILE_SIZE; i++)
		myState.regFile[i] = 0;
	if(instructions[0].opcode < CMD_NOP && instructions[0].opcode > CMD_MAX)
		return FAILURE;
	for(int i=0; i<SIM_PIPELINE_DEPTH; i++){
		myState.pipeStageState[i].cmd.opcode = CMD_NOP;
		myState.pipeStageState[i].cmd.src1 = 0;
		myState.pipeStageState[i].cmd.src2 = 0;
		myState.pipeStageState[i].cmd.isSrc2Imm = 0;
		myState.pipeStageState[i].cmd.dst = 0;
		myState.pipeStageState[i].src1Val = 0;
		myState.pipeStageState[i].src2Val = 0;
	}
	myState.pipeStageState[FETCH].cmd = instructions[0];
	return SUCCESS;
}


/**********************Core Cycle Management:*******************************/
	/*! SIM_CoreClkTick: Update the core simulator's state given one clock cycle.
  This function is expected to update the core pipeline given a clock cycle event.
*/
void SIM_CoreClkTick() {

	pipeStage shift;
	SIM_cmd_opcode opc_MEM = myState.pipeStageState[MEMORY].cmd.opcode;
	SIM_cmd_opcode opc_EX = myState.pipeStageState[EXECUTE].cmd.opcode;

	if (!memFail) { //if load is stuck, do not update PR registers
		PR_temp.exResult = PR.exResult; // saving last stage register reults before modifications to static var
		PR_temp.jump = PR.jump;
		PR_temp.readResult = PR.readResult;
		PR_temp.dstReg = PR.dstReg;
		PR_temp.lastUsedDstReg = PR.lastUsedDstReg;
	}


	StallDependencyType StallDep = detectDependency();
	FWDependencyType FWdep = detectForwardDependency();


	//first stages
	WriteBack();
	Execute();
	Decode();

	if (!forwarding) { // a stall should be inserted, if needed.
		switch (StallDep) {
			case (NO_DEP):
				if (Memory() == -1) {
					shift = WRITEBACK;
					memFail = 1;
				} else {
					shift = FETCH;
					memFail = 0;
				}
				break;
			case (DEC_WB):
				// FOR SPLIT
				if (split_regfile) {
					if (Memory() == -1) {
						shift = WRITEBACK;
						memFail = 1;
					} else {
						//STORE CASE
						if (checkDecodeStore() == 1)
							shift = EXECUTE;
						else
							shift = FETCH;
						memFail = 0;
					}
				} else { //STALL
					if (Memory() == -1) {
						shift = WRITEBACK;
						memFail = 1;
					} else {
						shift = EXECUTE;
						memFail = 0;
					}
				}
					break;
			default: //DEC_EXE or DEC_MEM or DEC_EXE_BRDST
				if (StallDep == DEC_EXE) {
					//cout << "DEC_EXE" << endl;
				}
				if (StallDep == DEC_MEM) {
					//cout << "DEC_MEM" << endl;
				}
				if (Memory() == -1) {
					shift = WRITEBACK;
					memFail = 1;
				} else {
					shift = EXECUTE;
					memFail = 0;
				}
				break;
		}
	}

	if (forwarding) { //a forward might be needed. only check what is the dependency, and move the pipe accordingly
		switch (FWdep) {
			case EXE_MEM_LOAD:
				if (Memory() == -1) {
					shift = WRITEBACK;
					memFail = 1;
				} else {
					shift = MEMORY;
					memFail = 0;
				}
				break;
			case EXE_NO_DEP:
				if (StallDep == DEC_EXE && myState.pipeStageState[EXECUTE].cmd.opcode == CMD_LOAD) { // a stall must be inserted in FW mode if LW is in EXE and DEC_EXE dependency occurs.
					if (Memory() == -1) {
						shift = WRITEBACK;
						memFail = 1;
					} else {
						shift = EXECUTE;
						memFail = 0;
					}
				} else {
					if (Memory() == -1) {
						shift = WRITEBACK;
						memFail = 1;
					} else {
						shift = FETCH;
						memFail = 0;
					}
				}
				break;
			default: //EXE_MEM_NOT_LOAD, EXE_WB
				if (Memory() == -1) {
					shift = WRITEBACK;
					memFail = 1;
				} else {
					shift = FETCH;
					memFail = 0;
				}
		}
	}

	SIM_cmd_opcode opcM = myState.pipeStageState[MEMORY].cmd.opcode;

	shiftPipe(shift); // pass on the pipe

	// FOR SPLIT or FW
	if ((split_regfile || forwarding) && opc_MEM != CMD_NOP && opc_MEM != CMD_HALT && !isJumpCommand(opcM)){ // : deleted opc_MEM != CMD_BREQ && opc_MEM != CMD_BR && opc_MEM != CMD_BRNEQ
		//cout << "Im splitting" << endl;
		if (myState.pipeStageState[WRITEBACK].cmd.dst != 0)
			Split();
	}

	//forward new values, according to dependency
	if(forwarding){
		FWDependencyType new_FWdep = detectForwardDependency();
		Forward(new_FWdep);
	}

} //SIM_CoreClkTick

/**********************Core State Getter:*******************************/
	/*! SIM_CoreGetState: Return the current core (pipeline) internal state
    curState: The returned current pipeline state
    The function will return the state of the pipe at the end of a cycle
*/
void SIM_CoreGetState(SIM_coreState *curState) {

	curState->pc = myState.pc;

	for(int i=0; i<SIM_PIPELINE_DEPTH; i++){
		curState->pipeStageState[i].src1Val = myState.pipeStageState[i].src1Val;
		curState->pipeStageState[i].src2Val = myState.pipeStageState[i].src2Val;
		curState->pipeStageState[i].cmd.src1 = myState.pipeStageState[i].cmd.src1;
		curState->pipeStageState[i].cmd.src2 = myState.pipeStageState[i].cmd.src2;
		curState->pipeStageState[i].cmd.dst = myState.pipeStageState[i].cmd.dst;
		curState->pipeStageState[i].cmd.isSrc2Imm = myState.pipeStageState[i].cmd.isSrc2Imm;
		curState->pipeStageState[i].cmd.opcode = myState.pipeStageState[i].cmd.opcode;
	}

	for(int i=0; i<SIM_REGFILE_SIZE; i++)
		curState->regFile[i] = myState.regFile[i];
}